# Extraction of prosodic feautres using Praat-parselmouth

`from prosodic_feature import PraatFeatures`

#### Input filepath
`feature=PraatFeatures()`

#### Get basic sound features
`feature.duration_energy_power("P4Q1.wav")`

#### Get Pitch features
`feature.pitch_features("P4Q1.wav")`

#### Get Frequnecy features
`feature.frequency_band("P4Q1.wav")`

#### Get Intensity features
`feature.intensity_features("P4Q1.wav")`

#### Get Jitter and shimmer features
`feature.jitter_shimmer("P4Q1.wav")`

#### Get all sound feature together
`feature.get_all_sound_feature("P4Q1.wav")`


*Please also see example_notebook*